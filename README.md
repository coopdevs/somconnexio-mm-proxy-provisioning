# Más Móvil API Proxy Provisioning

Provisioning scripts for https://gitlab.com/coopdevs/somconnexio-mm-proxy/

## Requirements

* Python 3.8+
* [Pyenv](https://github.com/pyenv/pyenv)

## Usage
Creating/Managing sysadmin users:

```
pyenv exec ansible-playbook playbooks/sys_admins.yml --limit=staging --ask-vault-pass -u root
```

Provisiong the app running:

```
pyenv exec ansible-playbook playbooks/provision.yml --limit=staging --ask-vault-pass
```

To deploy the app run:

```
pyenv exec ansible-playbook playbooks/deploy.yml --limit=staging
```

*Disclaimer:* this will run the app in development mode. It's just a first quick attempt at having something public reachable from the internet.

### Python version

We use [Pyenv](https://github.com/pyenv/pyenv) to fix the Python version and the virtualenv to develop the package.

You need to:

* Install and configure [`pyenv`](https://github.com/pyenv/pyenv)
* Install and configure [`pyenv-virtualenvwrapper`](https://github.com/pyenv/pyenv-virtualenvwrapper)
* Install the required Python version:

```
$ pyenv install 3.8.2
```

* Create the virtualenv:

```
$ pyenv virtualenv 3.8.2 somconnexio-mm-proxy-provisioning
```

### Python packages requirements

Install the Python packages in the virtual environment:

```
$ pyenv exec pip install -r requirements.txt
```
After installing Ansible, we need to download and install the project dependencies:

```
$ pyenv exec ansible-galaxy install -r requirements.yml
```

# Development environment

## Creating the VM

We use `Vagrant` to create and manage an isolated envionment to develop this project. First of all, follow the Vagrant installation steps to install the last version: https://www.vagrantup.com/docs/installation
> The Vagrant team doesn't recommend to use the package manager image: https://learn.hashicorp.com/vagrant/getting-started/install#caveats

We also need to install a VM engine. We use VirtualBox because it is Vagrant's default provider and because it supports boxes. Install [VirtualBox](https://www.virtualbox.org) before starting to run the Vagrant commands.
We have found some issues with the newest Virtualbox version (6.1.8) from the web when interacting with Valgrant, so we recommend to install with the package manager

```
$ sudo apt install virtualbox
```

### Create / Start the VM

```
$ vagrant up
```

### Stop the VM

```
$ vagrant halt
```

### Destroy the VM

```
$ vagrant destroy
```

### Reload the VM

If we do some modifications in the Vagrantfile, we can use the reload command to apply them:

```
$ vagrant reload
```

### Network configuration

We create a private network to assign a fixed private IP to our development environment.

The IP of the VM is `192.168.33.11`.

After starting a Vagrand machine you can run:

```
$ ssh root@192.168.33.11
```

If you prefer to use a URL instead of an IP, you can use the next command to add this IP to your `/etc/hosts` with the URL used in all the examples:

```
$ sudo -- sh -c "echo '192.168.33.11     somconnexio-mm-proxy.local' >> /etc/hosts"
```

Then you can try to run:

```
$ ssh root@somconnexio-mm-proxy.local
```

> :warning: If you remove your VM and recreate it, remember that you need to remove the known hosts entry:
> ```
> $ ssh-keygen -f "/home/<user>/.ssh/known_hosts" -R "192.168.33.11"
> $ ssh-keygen -f "/home/<user>/.ssh/known_hosts" -R "somconnexio-mm-proxy.local"
> ```

## Set up the server

After starting the app, we need to provision the server.

### Create System Administrators users - playbooks/sys_admins.yml

This playbook uses Coopdevs' [`sys-admins`role](https://github.com/coopdevs/sys-admins-role) to manage the system administrators users.

You can create a new folder with your domain as name inside host_vars (your domain needs to be declared as a host in the `inventory/hosts` file). In that new folder, modify these following vars, which you can find in file `config.yml`:

```
system_administrators_group:      # System administrators group
system_administrators:            # List of system administrators added to the group
  - name:                         # User name
    ssh_key:                      # User SSH public key file path
    state:                        # User state (present/absent)
```


The first time you run it against a brand new host you need to run it as `root` user. You'll also need passwordless SSH access to the root user.

After doing so, you can substitute `root` from the command for your personal user configured as SysAdmin.

```
$ pyenv exec ansible-playbook playbooks/sys_admins.yml --limit HOSTGROUP -u root
```

### Provision - playbooks/provision.yml

This playbook uses community and custom roles to install and configure all the dependencies in the server:

* Install the DB server
* Create DB user
* Create database
* Install the Python dependencies and pip
* Install some development tools (just in development enviornment)
* Set credentials for DB user and other variables as ENV VARS (allowing mm-proxy Django app to access them)
* Install docker
* Install and configure RabbitMQ inside a docker container
* Create systemd units for celery and django app jobs, and start running them as background processes (not in development envionment)


To executhe this playbook run:

```
$ pyenv exec ansible-playbook playbooks/provision.yml -u USER -l HOSTGROUP --ask-vault-pass
```

The `--ask-vault-pass` option will prompt for a password, which will ask for the one set when encrypting the variable. You can find the password in the Coopdevs BW with the name `mm-proxy-provisioning development - secrets vault encryption key ` or `mm-proxy-provisioning staging - secrets vault encryption key`.

USER is your nominal user.



### Start the app server (development environment)


To start the server in the development environment run:

```
$ ssh <user>@somconnexio-mm-proxy.local
$ sudo su - mm-proxy
$ pyenv activate somconnexio-mm-proxy
$ cd /var/www/somconnexio-mm-proxy/masmovil_proxy/
$ python manage.py runserver
```

### Start Celery (development environment)

In this project `celery` is used with `RabbitMQ` to deal with asynchronous tasks. To execute it, run:

```
$ ssh <user>@somconnexio-mm-proxy.local
$ sudo su - mm-proxy
$ pyenv activate somconnexio-mm-proxy
$ cd /var/www/somconnexio-mm-proxy/masmovil_proxy/
$ celery -A masmovil_proxy worker -l debug -Q mm-proxy
```

### Reset the DB / Enviroment

During development, the easiest way to totally reset the database or to restart building ansible from scratch is to delete the whole enviroment and buld it again:

```
$ vagrant status                         # Will show a list of existing containers
$ vagrant destroy                        # Stop and destroy the VM we want to recreate
```

Then, you can create again your environment starting back from point [Creating the VM](#creating-the-VM)

If you only need reset the db, you can execute the next command inside the enviroment with the `mm-proxy` user:

```
$ dropdb mm-proxy
$ createdb mm-proxy
```

And then we can start to run the migration and run the server.


## Deploy

We are using [Ansistrano](https://ansistrano.com/) to deploy our app.
Use the playbook `deploy.yml` to update the application project to the selected version:

```
$ pyenv exec ansible-playbook playbooks/deploy.yml --user USER --limit HOSTGROUP --ask-vault-pass
```

> TODO: How do we indicate the version we want to deploy?


### Staging/Production configuration

With the deployment done, the staging/production server will have the Django application and celery running as background jobs, with RabbitMQ listening.

Both the django app and celery are running under a systemd unit (`mm-proxy-gunicorn.service` and `celery-mm-proxy.service` respectively)

These services can be monitorized using the `systemctl` command. We can check the status of django or celery:
```
$ systemctl status mm-proxy-gunicorn.service
```
With sudo permissions and with the user mm-proxy, we can also stop or restart these services.

```
$ sudo systemctl stop mm-proxy-gunicorn.service
```
```
$ sudo systemctl restart mm-proxy-gunicorn.service
```

These commands above can be executed either with `mm-proxy-gunicorn.service` or `celery-mm-proxy.service`.

### Logs

#### Django

* Staging:
Django logs can be found in the path `/var/log/mm-proxy/app.log` or shown running this command:
```
$ sudo journalctl -exfu mm-proxy-gunicorn
```

* Development:
Django is executed manually. Therefore, its logs can be seen in the stdout.

#### Celery

* Staging:
Celery logs can be found in the path `/var/log/mm-proxy/mm-proxy-celery.log`.

* Development:
Celery is executed manually. Therefore, its logs can be seen in the stdout.

#### RabbitMQ
RabbitMQ logs can be checked executing this next [docker command](https://docs.docker.com/engine/reference/commandline/logs/), because RabbitMQ is running inside a docker container named 'rabbitmq':

```
$ docker logs rabbitmq
```

We can also know more about the RabbitMQ configuration, tasks process and queues with the RabbitMQ management tool:
* Staging:

  - Url: http://staging-mm-proxy.coopdevs.org:15672

  - Credentials: Bitwarden - "Som Connexió MM Proxy Staging - Rabbitmq credentials "

* Development:

  - Url: http://somconnexio-mm-proxy.local:15672

  - Credentials: Bitwarden - "Som Connexió MM Proxy Development - Rabbitmq credentials "


## Linting

To check the correct ansible structure and YAML syntax run the following commands, executing linters ([Ansible Lint](https://docs.ansible.com/ansible-lint/index.html), [yamllint](https://github.com/adrienverge/yamllint)) that will pass without warnings if everything is ok:

```
$ ansible-lint playbooks/*.yml
$ yamllint **/*.yml
```
